University of Cambridge Field Link
==================================

This module provides a `field_link` base link field which can be reused across
content types which have a 1-to-1 mapping between content and a URL, which
creates a consistent name for templates to use.

This ensures that standard functionality such as Carousels, Teaser lists etc
are provided by a common field that can be used in templates, etc.

Installing the module will create the `field_link` field. Uninstalling the
module does nothing - specifically, it does not delete the field.
